//
//  Servicio.swift
//  AppClimaGR1
//
//  Created by Sebastian Guerrero on 11/7/17.
//  Copyright © 2017 SG. All rights reserved.
//

import Foundation

class Servicio {
  
  func consultaPorCiudad (city:String, completion:@escaping (String) -> ()) {
    let urlStr = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=f4b14259b70ff9328f483a7729e0c980"
    
    consulta(urlStr: urlStr) { (weather, ciudad) in
      completion(weather)
    }
  }
  
  func consultaPorUbicacion (lat:Double, lon:Double, completion: @escaping (String) -> ()){
    let urlStr = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=f4b14259b70ff9328f483a7729e0c980"
    
    consulta(urlStr: urlStr) { (weather, ciudad) in
      completion(weather)
    }
  }
  
  func consulta(urlStr:String, completion:@escaping (String, String)-> ()){
    
    let url = URL(string: urlStr)
    let request = URLRequest(url: url!)
    
    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
      
      if let _ = error {
        return
      }
      
      do {
        
        let weatherJson = try JSONSerialization.jsonObject(with: data!, options: [])
        let weatherDict = weatherJson as! NSDictionary
        guard let weatherKey = weatherDict["weather"] as? NSArray else {
          completion("ciudad no valida", "ciudad")
          return
        }
        let weather = weatherKey[0] as! NSDictionary
        completion("\(weather["description"] ?? "no hay")", "ciudad")

      } catch {
        print("Error al generar el Json")
      }
    }
    task.resume()
  }
}
