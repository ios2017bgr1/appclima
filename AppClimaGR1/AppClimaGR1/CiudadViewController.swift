//
//  CiudadViewController.swift
//  AppClimaGR1
//
//  Created by Sebastian Guerrero on 10/31/17.
//  Copyright © 2017 SG. All rights reserved.
//

import UIKit

class CiudadViewController: UIViewController {
  
  //MARK:- Outlets
  
  @IBOutlet weak var cityTextField: UITextField!
  @IBOutlet weak var weatherLabel: UILabel!
  
  //MARK:- ViewController lifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  //MARK:- Actions
  
  @IBAction func consultarButtonPressed(_ sender: Any) {
    let service = Servicio()
    service.consultaPorCiudad(city: cityTextField.text!) { (weather) in
      DispatchQueue.main.async {
        self.weatherLabel.text = weather
      }
    }
  }
  
}













