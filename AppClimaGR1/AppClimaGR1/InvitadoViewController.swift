//
//  InvitadoViewController.swift
//  AppClimaGR1
//
//  Created by Sebastian Guerrero on 10/25/17.
//  Copyright © 2017 SG. All rights reserved.
//

import UIKit
import CoreLocation

class InvitadoViewController: UIViewController, CLLocationManagerDelegate {
  
  //MARK:- Outlets
  @IBOutlet weak var weatherLabel: UILabel!
  
  
  //MARK:- Attributes
  let locationManager = CLLocationManager()
//  var bandera = false
  
  //MARK:- ViewController LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    locationManager.requestWhenInUseAuthorization()
    
    if CLLocationManager.locationServicesEnabled() {
      
      locationManager.delegate = self
      locationManager.startUpdatingLocation()
      
    }
  }
  
  //MARK:- LocationManager Delegate
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    let location = manager.location?.coordinate
    //    if !bandera {
    consultarPorUbicación(
      lat: (location?.latitude)!,
      lon: (location?.longitude)!
    )
    //      bandera = true
    //    }
    locationManager.stopUpdatingLocation()
    
  }
  
  
  //MARK:- Actions
  private func consultarPorUbicación (lat:Double, lon:Double) {
    let service = Servicio()
    service.consultaPorUbicacion(lat: lat, lon: lon) { (weather) in
      DispatchQueue.main.async {
        self.weatherLabel.text = weather
      }
    }
  }
}
