//
//  ViewController.swift
//  AppClimaGR1
//
//  Created by Sebastian Guerrero on 10/25/17.
//  Copyright © 2017 SG. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  //MARK:- Outlets
  
  @IBOutlet weak var usernameTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  
  //MARK:- ViewController Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    view.endEditing(true)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    usernameTextField.text = ""
    passwordTextField.text = ""
    usernameTextField.becomeFirstResponder()
  }
  
  //MARK:- Actions
  
  @IBAction func entrarButtonPressed(_ sender: Any) {
    
    let user = usernameTextField.text ?? ""
    let password = passwordTextField.text ?? ""
    
    switch (user, password) {
    case ("sebas", "sebas"):
      performSegue(withIdentifier: "ciudadSegue", sender: self)
    case ("sebas", _):
      mostrarAlerta(mensaje: "Contraseña incorrecta")
    default:
      mostrarAlerta(mensaje: "usuario y contraseña incorrectas")
    }
  }
  
  private func mostrarAlerta(mensaje:String) {
    let alertView = UIAlertController(title: "Error", message: mensaje, preferredStyle: .alert)
    
    let aceptar = UIAlertAction(title: "Aceptar", style: .default) { (action) in
      self.usernameTextField.text = ""
      self.passwordTextField.text = ""
    }
    
    alertView.addAction(aceptar)
    
    present(alertView, animated: true, completion: nil)
  }
  
  
  @IBAction func invitadoButtonPressed(_ sender: Any) {
  }
  
  
}

